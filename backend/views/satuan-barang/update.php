<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\SatuanBarang */

$modelName = 'Satuan Barang';
$this->title = 'Update Satuan Barang | ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Satuan Barang', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="box-header">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3> 
</div>
<div class="box-body">
    <div class="row">
        <div class="col-lg-12">

            <div class="satuan-barang-update">

                <?= $this->render('_form', [
                'model' => $model,
                ]) ?>

            </div>

        </div>
    </div>
</div>