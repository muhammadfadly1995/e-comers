<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SatuanBarangSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Satuan Barang';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box-header">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    <div class="pull-right">
        <a class="btn btn-primary btn-sm" href="<?= Url::toRoute(['create']) ?>">Tambahkan Data</a>
    </div>
</div>
<div class="box">
    <div class="box-body">
    <ul class="nav nav-tabs">
            <li class=""><?= Html::a('Data Barang', ['/barang']) ?></li>

            <li class=""><?= Html::a('Satuan Barang', ['/satuan-barang']) ?></li>

            <li class=""><?= Html::a('Kategori Barang', ['/kategori-barang']) ?></li>
            <li class=""><?= Html::a('Pesanan Barang', ['/barang/pesanan']) ?></li>
            <li class=""><?= Html::a('Menu Barang', ['/menu-barang']) ?></li>
           
            


        </ul>

        <div class="row">
            <div class="col-lg-12">
                <div class="satuan-barang-index">
                    <?php // echo $this->render('_search', ['model' => $searchModel]); 
                    ?>

                    <div class="table-responsive">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => [
                                [
                                    'class' => 'yii\grid\SerialColumn',
                                    'header' => 'No.'
                                ],

                                //   'id',
                                'keterangan',

                                ['class' => 'yii\grid\ActionColumn'],
                            ],
                        ]); ?>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>