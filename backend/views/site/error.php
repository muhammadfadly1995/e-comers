<?php
/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = '#' . $exception->statusCode;
?>

<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="site-error">

                            <div class="alert alert-danger">
                                <?= nl2br(Html::encode($exception->getMessage())) ?>
                            </div>

                            <p>
                                The above error occurred while the Web server was processing your request.
                            </p>
                            <p>
                                Please contact us if you think this is a server error. Thank you.
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>