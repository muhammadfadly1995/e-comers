<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TabelSekolahSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


?>

<div class="box-header">
    
     <div class="pull-left">
        <a class="btn btn-primary btn-sm" href="<?= Url::toRoute(['/site/master-aset']) ?>">Master Aset</a>
        <a class="btn btn-primary btn-sm" href="<?= Url::toRoute(['/site/inventarisasi']) ?>">Inventarisasi</a>
        <a class="btn btn-primary btn-sm" href="<?= Url::toRoute(['/site/stock-opname']) ?>">Stock Opname</a>
        


    </div>
   
</div>
