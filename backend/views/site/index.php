<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'E-COMERS';
?>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Welcome to Dashboard</h3>
            </div>
            <div class="box-body">
                <div class="box-widget widget-user">
                    <div class="widget-user-header" style="background-color: #7fc6b6;">
                        <h3 class="widget-user-username" style="color: white;"><?= Yii::$app->user->identity->name ?></h3>
                        <h5 class="widget-user-desc" style="color: white;">Administrator</h5>
                    </div>
                    <div class="widget-user-image">
                        <img class="img-circle" src="<?= Url::toRoute(['/site/profile-photo', 'inline' => true]) ?>" style="height: 90px; max-height: 90px; background-color: #333333;" alt="User Avatar">
                    </div>
                    <div class="box-footer" style="padding-bottom:0px">
                        <div class="row">
                            <center><br>
                                <p style="width:90%">Berikut data informasi detail akun anda saat ini : </p>
                            </center>
                        </div>
                    </div>
                </div>
                <div class="col-lg-offset-3 col-lg-6">
                    <div class="row">
                        <div class="col-lg-4 text-bold">Name</div>
                        <div class="col-lg-8"><?= Yii::$app->user->identity->name ?></div>
                        <div class="col-lg-4 text-bold">Username</div>
                        <div class="col-lg-8"><?= Yii::$app->user->identity->username ?></div>
                        <div class="col-lg-4 text-bold">Email</div>
                        <div class="col-lg-8"><?= Yii::$app->user->identity->email ?></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-lg-12">
                            <a class="btn btn-block btn-sm btn-primary" href="<?= Url::toRoute(['/site/profile']) ?>">Profil</a><br>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>