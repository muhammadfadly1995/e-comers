<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
.w3-button {width:150px}
</style>
<div style="padding-bottom:15px">
     <img class="img-responsive center-block" src="<?= Yii::getAlias('@web/background/logo2.jpg') ?>" alt="logo polda" width="80px;">
</div>
<h4 class="login-box-msg">
    Login Sistim E-Comers
 
</h4>
<div class="site-login">
    <?php
    $form = ActiveForm::begin([
        'id' => 'login-form',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'options' => [
                'label' => 'col-sm-4',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-8',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]);
    ?>
   <div class="col-md-12">
    <div class="has-feedback">
        <?= $form->field($model, 'username')->textInput(['class' => 'form-control', 'placeholder' => 'Username'])->label(false) ?>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
    </div>
    </div>
    <div class="col-md-12">
    <div class="has-feedback">
        <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control', 'placeholder' => 'Password'])->label(false) ?>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
    </div>
    </div>
    <div class="col-md-12">
        
        <div class="col-md-5">
    <div class="has-feedback">
        <?= $form->field($model, 'bilangan_pertama')->textInput(['class' => 'form-control', 'placeholder' => 'Hasil Pertambahan','readOnly'=>true])->label(false) ?>
        
    </div>
    </div>
     <div class="col-md-2">
    <div class="has-feedback">
        +
       </div>
    </div>
    <div class="col-md-5">
    <div class="has-feedback">
        <?= $form->field($model, 'bilangan_kedua')->textInput(['class' => 'form-control', 'placeholder' => 'Hasil Pertambahan','readOnly'=>true])->label(false) ?>
       </div>
    </div>
    </div>
    
    <div class="col-md-12">
    <div class="has-feedback">
        <?= $form->field($model, 'bilangan_hasil')->textInput(['class' => 'form-control', 'placeholder' => 'Hasil Pertambahan'])->label(false) ?>
       
    </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6">
            <?= Html::submitButton('Login', ['class' => 'w3-button w3-border w3-hover-blue', 'name' => 'login-button']) ?>
            </div><div class="col-md-6">
                <a href="./tambah-baru" class="w3-button w3-border w3-hover-blue" role="button" data-target="#modalvote" data-toggle="modal">Signup</a>
           
        </div>
     
    </div>
    </div>
    <div class="row">
        <div class="col-xs-12 text-center">
            <br />
            <a href="<?= Url::toRoute(['/site/request-password-reset']) ?>">- Reset Password -</a>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    

</div>
 <div class="modal remote fade" id="modalvote">
        <div class="modal-dialog">
            <div class="modal-content loader-lg"></div>
        </div>
</div>