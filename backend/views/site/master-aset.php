<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel common\models\KartuKontrolSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kartu Kontrol';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php include('header.php');?>
<div class="box">
    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>

        <div class="col-md-5">
            <?=
            $form->field($model, 'no_dipa')->widget(\kartik\select2\Select2::classname(), [
                'data' => ArrayHelper::map(common\models\KartuKontrol::find()->groupBy('no_dipa')->asArray()->all(), 'no_dipa', 'no_dipa'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>




        </div>
        <div class="col-md-4">
            <?=
            $form->field($model, 'sumber_dana')->label('Sumber Dana')->widget(\kartik\select2\Select2::classname(), [
                'data' => ArrayHelper::map(common\models\KartuKontrol::find()->groupBy('sumber_dana')->asArray()->all(), 'sumber_dana', 'sumber_dana'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-md-3">
            <br>
            <?= Html::submitButton('Cari Data', ['class' => 'btn btn-primary', 'name' => 'simpan', 'value' => 'true']) ?>
            <?= Html::submitButton('Print Data', ['class' => 'btn btn-primary', 'name' => 'cetak', 'value' => 'true', 'target' => '_Blank']) ?>

        </div>
        <?php ActiveForm::end(); ?>
    </div>
    </div>

<div class="box">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <div class="box-header">
                    <h3 class="box-title"><?= 'Master Aset' ?></h3>
                </div>
                <table class="table table-bordered table-hover table-responsive table-striped" cellpadding="6" cellspacing="1" style="width:100%" border="1" >
                    <thead style="background-color: #7fc6b6;">

                        <tr>
                            <th width="3%">No</th>
                            <th>Lokasi</th>
                            <th>Luas (M2)</th>
                            <th>Nilai Aset Per (M2)</th>
                            <th>Nilai Aset Keseluruhan</th>
                            <th>Keterangan</th>
                            
                            





                        </tr>
                    </thead >
                    
                    <?php // foreach($daftarkartukontrol as $key=>$val){
//                        $datastatus='-';
//                        $cekdataadministrasi=\common\models\AdministrasiPengadaan::find()->where(['id_cara_pengadaan'=>$val->cara_pengadaan])->count('keterangan');
//                        $cekdatakontrakadministrasi=  \common\models\KartuKontrolAdministrasi::find()->where(['id_kartu_kontrol'=>$val->id])->count('keterangan');
//                        $carapengadaan=  \common\models\CaraPengadaan::find()->where(['id'=>$val->cara_pengadaan])->one();
//                       $datasiup=  \common\models\Siup::find()->where(['jenis_siup'=>$val->siup])->orderBy('id DESC')->one();
//                        // print_r($cekdataadministrasi);die();
//                        if($cekdataadministrasi == $cekdatakontrakadministrasi){
//                            $datastatus=='Selesai';
//                        }else{
//                            $datastatus='Dalam Proses';
//                        }
//                        
//                        $no=1;
                       ?>
                    <tr>
                        <td><?php //= $no+$key; ?></td>
 
                        <td class="text-left"><?php //echo $val->no_rup; ?></td>
                        <td class="text-left"><?php //echo $val->nama_paket; ?></td>
                        <td class="text-left"><?php //echo $val->sumber_dana; ?></td>
                        <td class="text-left"><?php //echo $carapengadaan->cara_pengadaan; ?></td>
                        <td class="text-left"><?php //echo $val->no_dipa; ?></td>
                        <td class="text-left"><?php //echo $val->no_mak; ?></td>
                        
                    </tr>
                    <?php // }?>
                </table>
            </div>
        </div>
    </div>
</div>