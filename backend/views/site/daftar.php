<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use kartik\money\MaskMoney;
?>
<?php
$form = ActiveForm::begin();
?>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
.w3-button {width:150px}
</style>

<div class="modal-header" style="background-color:#d2ddde ">
    <button type="button" class="close" data-dismiss="modal">&times;</button>

</div>
<div style="padding-bottom:15px">
     <img class="img-responsive center-block" src="<?= Yii::getAlias('@web/background/logo2.png') ?>" alt="logo polda" width="80px;">
</div>
<h4 class="login-box-msg">
    Signup Sistim Pengadaan
 
</h4>
<div class="modal-body">
    <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

    <?= $form->field($model, 'email') ?>
    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'password')->passwordInput() ?>

</div>
<div class="modal-footer" style="background-color:#d2ddde ">
    <?= Html::submitButton('Simpan', ['class' => 'w3-button w3-border w3-hover-blue']) ?>
    <button type="button" class="w3-button w3-border w3-hover-red" data-dismiss="modal">Close</button>
</div>
<?php ActiveForm::end(); ?>