<?php
/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\helpers\StringHelper;
use yii\bootstrap\Modal;

AppAsset::register($this);

$user = Yii::$app->user->identity;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?= Yii::$app->name ?>">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="shortcut icon" type="image/x-icon" href="<?= yii\helpers\Url::toRoute(['/logo.ico']) ?>" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<?= yii\helpers\Url::toRoute(['/bower_components/datatables.net/js/jquery.dataTables.min.js']) ?>"></script>
    <script src="<?= yii\helpers\Url::toRoute(['/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js']) ?>"></script>
     <style>
        .select2-container .select2-selection--single .select2-selection__rendered {
            margin-top: 0px;
        }
    </style>
</head>

<body class="hold-transition skin-yellow sidebar-mini fixed">
    <?php $this->beginBody() ?>
    <div class="wrapper">
        <header class="main-header">
            <!-- Logo -->
            <a href="<?= Url::toRoute(['/']) ?>" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"> <i class="fa fa-home"></i></span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-sm" style="font-size:16px;"><?= 'E-COMERS' ?></span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?= Url::toRoute(['/site/profile-photo', 'inline' => true]) ?>" class="user-image" alt="User Image" style="background-color: #d8f1ec;">
                                <span class="hidden-xs"><?= ucwords($user->name) ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="<?= Url::toRoute(['/site/profile-photo', 'inline' => true]) ?>" class="img-circle" alt="User Image" style="background-color: #333333;">

                                    <p>
                                        <small>Member since <?= date("Y M d", $user->created_at) ?></small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="<?= Url::toRoute(['/site/profile']) ?>" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="<?= Url::to(['/site/logout']) ?>" data-method="post" class="btn btn-default btn-flat">Log Out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <!-- =============================================== -->

        <!-- Left side column. contains the sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="<?= Url::toRoute(['/site/profile-photo', 'inline' => true]) ?>" class="img-circle" alt="User Image" style="background-color: #d8f1ec;">
                    </div>
                    <div class="pull-left info">
                        <p><?= ucwords($user->name) ?></p>
                        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                    </div>
                </div>
                <br>
                <!-- sidebar menu: : style can be found in sidebar.less -->
<!--                <ul class="sidebar-menu">
                    <li class="header">MAIN NAVIGATION</li>
                    <li <?= Yii::$app->controller->id == 'site' ? 'class=active' : '' ?>>
                        <a href="<?= Url::toRoute(['/']) ?>">
                            <i class="fa fa-home text-yellow"></i> <span>Home</span>
                        </a>
                    </li>
                      <li class="header">MASTER NAVIGATION</li>
                    <li <?= Yii::$app->controller->id == 'cara-pengadaan' ? 'class=active' : '' ?>>
                        <a href="<?= Url::toRoute(['/cara-pengadaan']) ?>">
                            <i class="fa fa-users text-yellow"></i> <span>Cara Pengadaan</span>
                        </a>
                    </li>
                    <li <?= Yii::$app->controller->id == 'data-barang' ? 'class=active' : '' ?>>
                        <a href="<?= Url::toRoute(['/data-barang']) ?>">
                            <i class="fa fa-users text-yellow"></i> <span>Data Barang</span>
                        </a>
                    </li>
                       <li <?= Yii::$app->controller->id == 'data-pegawai' ? 'class=active' : '' ?>>
                        <a href="<?= Url::toRoute(['/data-pegawai']) ?>">
                            <i class="fa fa-users text-yellow"></i> <span>Data Pegawai</span>
                        </a>
                    </li>
                      <li <?= Yii::$app->controller->id == 'jabatan' ? 'class=active' : '' ?>>
                        <a href="<?= Url::toRoute(['/jabatan']) ?>">
                            <i class="fa fa-users text-yellow"></i> <span>Jabatan</span>
                        </a>
                    </li>
                      <li <?= Yii::$app->controller->id == 'jenis-barang' ? 'class=active' : '' ?>>
                        <a href="<?= Url::toRoute(['/jenis-barang']) ?>">
                            <i class="fa fa-users text-yellow"></i> <span>Jenis Barang</span>
                        </a>
                    </li>
                     <li <?= Yii::$app->controller->id == 'satuan-barang' ? 'class=active' : '' ?>>
                        <a href="<?= Url::toRoute(['/satuan-barang']) ?>">
                            <i class="fa fa-users text-yellow"></i> <span>Satuan Barang</span>
                        </a>
                    </li>
                    <li <?= Yii::$app->controller->id == 'master-bank' ? 'class=active' : '' ?>>
                        <a href="<?= Url::toRoute(['/master-bank']) ?>">
                            <i class="fa fa-users text-yellow"></i> <span>Master Bank</span>
                        </a>
                    </li>
                    <li <?= Yii::$app->controller->id == 'sumber-dana' ? 'class=active' : '' ?>>
                        <a href="<?= Url::toRoute(['/sumber-dana']) ?>">
                            <i class="fa fa-users text-yellow"></i> <span>Sumber Dana</span>
                        </a>
                    </li>
                    <li <?= Yii::$app->controller->id == 'tim-teknis' ? 'class=active' : '' ?>>
                        <a href="<?= Url::toRoute(['/tim-teknis']) ?>">
                            <i class="fa fa-users text-yellow"></i> <span>Tim Teknis</span>
                        </a>
                    </li>
                    <li <?= Yii::$app->controller->id == 'vendor' ? 'class=active' : '' ?>>
                        <a href="<?= Url::toRoute(['/vendor']) ?>">
                            <i class="fa fa-users text-yellow"></i> <span>Vendor</span>
                        </a>
                    </li>
                    <li <?= Yii::$app->controller->id == 'tabel-anggota' ? 'class=active' : '' ?>>
                        <a href="<?= Url::toRoute(['/tabel-anggota']) ?>">
                            <i class="fa fa-users text-yellow"></i> <span>Tabel Anggota</span>
                        </a>
                    </li>
                    
                     <li class="header">Pengadaan</li>
                    <li <?= Yii::$app->controller->id == 'pengadaan-barang' ? 'class=active' : '' ?>>
                        <a href="<?= Url::toRoute(['/pengadaan-barang']) ?>">
                            <i class="fa fa-users text-yellow"></i> <span>Pengadaan Barang</span>
                        </a>
                    </li>
                    
                     <li class="header">RAB</li>
                    <li <?= Yii::$app->controller->id == '/pengadaan-barang/rab' ? 'class=active' : '' ?>>
                        <a href="<?= Url::toRoute(['/pengadaan-barang/rab']) ?>">
                            <i class="fa fa-users text-yellow"></i> <span>RAB</span>
                        </a>
                    </li>
                    <li class="header">Kartu Kontrol</li>
                    <li <?= Yii::$app->controller->id == '/kartu-kontrol' ? 'class=active' : '' ?>>
                        <a href="<?= Url::toRoute(['/kartu-kontrol']) ?>">
                            <i class="fa fa-users text-yellow"></i> <span>Kartu Kontrol</span>
                        </a>
                    </li>
                    
                    
                    <li class="header">USER NAVIGATION</li>
                    <li <?= Yii::$app->controller->id == 'user' ? 'class=active' : '' ?>>
                        <a href="<?= Url::toRoute(['/user']) ?>">
                            <i class="fa fa-users text-yellow"></i> <span>User</span>
                        </a>
                    </li>
                </ul>-->
                
                <ul class="sidebar-menu">
                    <li class="header">DATA MASTER</li>
                    <li <?= Yii::$app->controller->id == '/' ? 'class=active' : '' ?>>
                        <a href="<?= Url::toRoute(['/barang']) ?>">
                            <i class="fa fa-users text-yellow"></i> <span>Data Master</span>
                        </a>
                    </li>
                    
                    
                     
                    
                    
                   
                    
                </ul>
               
                
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            <!-- Main content -->
            <section class="content">

                <div class="row">
                    <div class="col-lg-12">
                        <?php if (Yii::$app->session->getFlash('success')) : ?>
                            <div id="w0-success-0" class="alert alert-success" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <?= Yii::$app->session->getFlash('success') ?>
                            </div>
                        <?php elseif (Yii::$app->session->getFlash('error')) : ?>
                            <div id="w0-error-0" class="alert alert-danger" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <?= Yii::$app->session->getFlash('error') ?>
                            </div>
                        <?php elseif (Yii::$app->session->getFlash('warning')) : ?>
                            <div id="w0-error-0" class="alert alert-warning" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <?= Yii::$app->session->getFlash('warning') ?>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
 
                <?= $content ?> 

            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 1.1.5
            </div>
            <strong>Copyright &copy; <?= date('Y') ?> <a href="<?= Url::toRoute(['/']) ?>"><?= '-' ?></a>  </strong>
        </footer>

    </div>

    <script>
        $('th:contains("No.")').css('width', '10px');
        $('th.action-column').css('width', '30px');
    </script>

    <!--jangan diganggu-->
    <?php
    Modal::begin([
        'header' => 'janganDiganggu',
        'id' => 'modalJanganDiganggu',
        'size' => 'modal-lg',
    ]);

    echo "<div id='janganDiganggu'></div>";

    Modal::end();
    ?>
    <!--jangan diganggu-->

    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>