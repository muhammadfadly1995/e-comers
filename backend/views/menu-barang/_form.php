<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MenuBarang */
/* @var $form yii\bootstrap\ActiveForm */
?>
<div class="box">
    <div class="box-body">
        <div class="menu-barang-form">

            <?php $form = ActiveForm::begin([
                //    'layout' => 'horizontal',
            ]); ?>

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'keterangan')->textInput(['maxlength' => true]) ?>

                </div>
                <div class="col-md-6">
                    <div class="col-md-12">
                        <?= $form->field($model, 'imageFiles')->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>


                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    <a href="./index" class="btn btn-default">Kembali</a>
                </div>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>