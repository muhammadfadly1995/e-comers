<?php

use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model common\models\MenuBarang */

$modelName = 'Menu Barang';
$this->title = 'Input Menu Barang';
$this->params['breadcrumbs'][] = ['label' => 'Menu Barang', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box-header">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3> 
</div>
<div class="box-body">
    <div class="row">
        <div class="col-lg-12">

            <div class="admin-create">

                <div class="menu-barang-create">

                    <?= $this->render('_form', [
                    'model' => $model,
                    ]) ?>

                </div>


            </div>
        </div>
    </div>
</div>