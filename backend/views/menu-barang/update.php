<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\MenuBarang */

$modelName = 'Menu Barang';
$this->title = 'Update Menu Barang | ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Menu Barang', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="box-header">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3> 
</div>
<div class="box-body">
    <div class="row">
        <div class="col-lg-12">

            <div class="menu-barang-update">

                <?= $this->render('_form', [
                'model' => $model,
                ]) ?>

            </div>

        </div>
    </div>
</div>