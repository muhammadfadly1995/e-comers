<?php

use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model backend\models\User */

$modelName = 'User';
$this->title = 'Input User';
$this->params['breadcrumbs'][] = ['label' => 'User', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-12">

                        <div class="admin-create">

                            <div class="user-create">

                                <?= $this->render('_form', [
                                    'model' => $model,
                                ]) ?>

                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>