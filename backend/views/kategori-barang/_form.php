<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\KategoriBarang */
/* @var $form yii\bootstrap\ActiveForm */
?>
<div class="box">
    <div class="box-body">
        <div class="kategori-barang-form">

            <?php $form = ActiveForm::begin([
                //  'layout' => 'horizontal',
            ]); ?>

            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'keterangan')->textInput(['maxlength' => true]) ?>

                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    <a href="./index" class="btn btn-default">Kembali</a>
                </div>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>