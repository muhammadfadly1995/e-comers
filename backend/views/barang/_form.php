<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\money\MaskMoney;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\Barang */
/* @var $form yii\bootstrap\ActiveForm */
?>
<div class="box">
    <div class="box-body">
        <div class="barang-form">

            <?php $form = ActiveForm::begin([
                // 'layout' => 'horizontal',
            ]); ?>

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'nama_barang')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'menu_barang')->widget(\kartik\select2\Select2::classname(), [
                'data' => ArrayHelper::map(common\models\MenuBarang::find()->asArray()->all(), 'id', 'keterangan'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>

                    <?= $form->field($model, 'satuan')->widget(\kartik\select2\Select2::classname(), [
                'data' => ArrayHelper::map(common\models\SatuanBarang::find()->asArray()->all(), 'id', 'keterangan'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih Satuan...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
             
                  
                   
                </div><div class="col-md-6">
                <?= $form->field($model, 'id_kategori')->widget(\kartik\select2\Select2::classname(), [
                'data' => ArrayHelper::map(common\models\KategoriBarang::find()->asArray()->all(), 'id', 'keterangan'),
                'language' => 'de',
                'options' => ['placeholder' => 'Pilih Satuan...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
                <?= $form->field($model, 'merk_barang')->textInput(['maxlength' => true]) ?>
               
                   

                   
                   

                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'imageFiles')->label('Latar Foto')->fileInput(['multiple' => false,'accept' => 'image/*']) ?>


                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    <a href="./index" class="btn btn-default">Kembali</a>
                </div>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>