<?php

use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model common\models\Barang */

$modelName = 'Barang';
$this->title = 'Input Barang';
$this->params['breadcrumbs'][] = ['label' => 'Barang', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box-header">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
</div>
<div class="box-body">
    <div class="row">
        <div class="col-lg-12">

            <div class="admin-create">

                <div class="barang-create">

                    <?= $this->render('_form', [
                        'modelspesifikasibarang' => $modelspesifikasibarang,
                        'model' => $model,
                    ]) ?>

                </div>


            </div>
        </div>
    </div>
</div>