<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\money\MaskMoney;
/* @var $this yii\web\View */
/* @var $model common\models\Barang */

$modelName = 'Tambah Keterangan Ukuran Barang';
$this->title = 'Tambah Keterangan Ukuran Barang| ' . $dataukuran->ukuran;
$this->params['breadcrumbs'][] = ['label' => 'Barang', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box-header">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
</div>
<div class="box">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <div class="box-header">
                    <h3 class="box-title"><?= 'Data Barang' ?></h3>
                </div>
                <table class="table table-bordered table-hover table-responsive table-striped" cellpadding="6" cellspacing="1" style="width:100%" border="1">
                    <thead style="background-color: #7fc6b6;">

                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>

                            <th>Ukuran Barang</th>
                            <th>Harga Beli Barang</th>
                            <th>Harga Jual Barang</th>
                            <th>Jumlah Barang</th>


                        </tr>
                    </thead>

                    <?php
                    $no = 1;
                    if ($dataukuran) {
                        $databarang = \common\models\Barang::find()->where(['id' => $dataukuran->id_barang])->one();
                    ?>
                        <tr>
                            <td><?= $no; ?></td>
                            <td><?= $databarang->nama_barang; ?></td>

                            <td><?= $dataukuran->ukuran; ?></td>
                            <td><?php echo 'Rp.' . number_format($dataukuran->harga_beli); ?></td>
                            <td><?php echo 'Rp.' . number_format($dataukuran->harga); ?></td>
                            <td><?= $dataukuran->jumlah_barang; ?></td>


                        </tr>
                    <?php } ?>

                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="box-header">
                    <h3 class="box-title"><?= 'Keterangan ' ?></h3>
                </div>
                <table class="table table-bordered table-hover table-responsive table-striped" cellpadding="6" cellspacing="1" style="width:100%" border="1">
                    <thead style="background-color: #7fc6b6;">

                        <tr>
                            <th width="5%">No</th>
                            <th width="25%">Warna Barang</th>
                            <th width="25%">Keterangan Lainnya</th>

                           
                        </tr>
                    </thead>

                    <?php
                    $no = 1;
                    foreach ($dataketerangan as $key => $val) {
                    ?>
                        <tr>
                            <td><?= $no + $key; ?></td>

                            <td class="text-left"><?php echo $val->keterangan_warna; ?></td>
                            <td class="text-left"><?php echo $val->keterangan_lainya; ?></td>
                            </td>





                        </tr>
                    <?php }
                    ?>
                </table>
            </div>
            <div class="col-md-6">
                <div class="box-header">
                    <h3 class="box-title"><?= 'Data Foto' ?></h3>
                </div>
                <table class="table table-bordered table-hover table-responsive table-striped" cellpadding="6" cellspacing="1" style="width:100%" border="1">
                    <thead style="background-color: #7fc6b6;">

                    <tr>
                            <th width="5%">No</th>
                            <th width="45%">Warna Barang</th>
                            <th width="50%">Foto</th>

                           
                        </tr>
                    </thead>

                    <?php
                    $nofoto = 1;
                    foreach ($dataketerangan as $key => $val) {
                        $datafoto=\common\models\FotoBarang::find()->where(['id_keterangan'=>$val->id])->all();
                        foreach($datafoto as $keyfoto =>$valfoto){
                            $imgPath = Yii::getAlias('@web/img/fotobarang/');
                            $img = is_file(Yii::getAlias('@webroot/img/fotobarang/').$valfoto->foto) 
                            ? $imgPath.$valfoto->foto 
                            : $imgPath.$valfoto->foto;
                          
               
                            
                       
                    ?>
                    
                        <tr>
                            <td><?= $nofoto ++; ?></td>

                            <td class="text-left"><?php echo $val->keterangan_warna; ?></td>
                            <td class="text-left"><img class="img-responsive" src="<?= $img?>" alt="Girl in a jacket" width="150" height="100"></td>
                             </td>





                        </tr>
                        <?php }?>
                    <?php }
                    ?>
                </table>
            </div>
        </div>



    </div>
</div>