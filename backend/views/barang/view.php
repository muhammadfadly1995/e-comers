<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\money\MaskMoney;
/* @var $this yii\web\View */
/* @var $model common\models\Barang */

$modelName = 'Barang';
$this->title = 'Barang | ' . $model->nama_barang;
$this->params['breadcrumbs'][] = ['label' => 'Barang', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box-header">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
</div>
<div class="box">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <div class="box-header">
                    <h3 class="box-title"><?= 'Data Barang' ?></h3>
                </div>
                <table class="table table-bordered table-hover table-responsive table-striped" cellpadding="6" cellspacing="1" style="width:100%" border="1">
                    <thead style="background-color: #7fc6b6;">

                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>

                            <th>Satuan Barang</th>
                            <th>Kategori Barang</th>
                            <th>Jumlah Barang</th>
                            <th>Merk Barang</th>


                        </tr>
                    </thead>

                    <?php
                    $no = 1;
                    if ($model)
                        $datasatuan = \common\models\SatuanBarang::find()->where(['id' => $model->satuan])->one();
                    $datakategori = \common\models\KategoriBarang::find()->where(['id' => $model->id_kategori])->one(); { ?>

                        <tr>
                            <td><?= $no; ?></td>
                            <td><?= $model->nama_barang; ?></td>

                            <td><?= $datasatuan->keterangan; ?></td>
                            <td><?= $datakategori->keterangan; ?></td>
                            <td><?= $model->jumlah_barang; ?></td>
                            <td><?= $model->merk_barang; ?></td>


                        </tr>
                    <?php } ?>

                </table>
            </div>
        </div>


        <div class="row">
            <div class="col-md-6">
                <div class="barang-form">

                    <?php
                    $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>


                    <div class="col-md-6">
                        <?= $form->field($modelukuran, 'ukuran')->textInput(['maxlength' => true]) ?>

                    </div>
                    <div class="col-md-6">
                        <?= $form->field($modelukuran, 'jumlah_barang')->textInput(['maxlength' => true]) ?>

                    </div>
                    <div class="col-md-6">
                        <?= $form->field($modelukuran, 'harga_beli')->widget(
                            MaskMoney::classname(),
                            [
                                'options' => [
                                    // 'readOnly' => true,
                                    //  'id' => 'harga-dp'
                                ],
                                'pluginOptions' => [
                                    'prefix' => 'Rp. ',
                                    'suffix' => '',
                                    'affixesStay' => TRUE,
                                    'precision' => 0,
                                    'allowZero' => false,
                                    'allowNegative' => false,
                                    'placeholder' => 'HPS',
                                    //'readOnly'=>true,
                                ],
                            ]
                        );
                        ?>


                    </div>
                    <div class="col-md-6">
                        <?= $form->field($modelukuran, 'harga')->widget(
                            MaskMoney::classname(),
                            [
                                'options' => [
                                    // 'readOnly' => true,
                                    //  'id' => 'harga-dp'
                                ],
                                'pluginOptions' => [
                                    'prefix' => 'Rp. ',
                                    'suffix' => '',
                                    'affixesStay' => TRUE,
                                    'precision' => 0,
                                    'allowZero' => false,
                                    'allowNegative' => false,
                                    'placeholder' => 'HPS',
                                    //'readOnly'=>true,
                                ],
                            ]
                        );
                        ?>


                    </div>
                </div>

                <div class="col-md-6">
                    <?= $form->field($modelketeranganukuran, 'keterangan_warna')->textInput(['maxlength' => true]) ?>

                </div>
                <div class="col-md-6">
                    <?= $form->field($modelketeranganukuran, 'keterangan_lainya')->textInput(['maxlength' => true]) ?>

                </div>
                <div class="col-md-12">
                    <?= $form->field($modelfoto, 'imageFiles[]')->fileInput(['multiple' => true,'accept' => 'image/*']) ?>


                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= Html::submitButton('Simpan', ['class' => 'btn btn-primary', 'name' => 'simpan', 'value' => 'true']) ?>
                        <a href="./index" class="btn btn-default">Selesai</a>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
            <div class="col-md-6">
                <div class="box-header">
                    <h3 class="box-title"><?= 'Keterangan Ukuran Barang' ?></h3>
                </div>
                <table class="table table-bordered table-hover table-responsive table-striped" cellpadding="6" cellspacing="1" style="width:100%" border="1">
                    <thead style="background-color: #7fc6b6;">

                        <tr>
                            <th width="5%">No</th>
                            <th width="25%">Ukuran Barang</th>
                            <th width="25%">Harga Beli</th>
                            <th width="25%">Harga Jual</th>
                            <th width="25%">Jumlah Barang</th>
                            <th width="25%">Action</th>
                        </tr>
                    </thead>

                    <?php
                    $no = 1;
                    foreach ($dataukuran as $key => $val) {
                    ?>
                        <tr>
                            <td><?= $no + $key; ?></td>

                            <td class="text-left"><?php echo $val->ukuran; ?></td>
                            <td class="text-left"><?php echo 'Rp.' . number_format($val->harga_beli); ?></td>
                            <td class="text-left"><?php echo 'Rp.' . number_format($val->harga); ?></td>
                            <td class="text-left"><?php echo $val->jumlah_barang; ?></td>
                            <td class="text-left"><a href="<?php echo \yii\helpers\Url::to(['/barang/tambah-keterangan', 'id' => $val->id,'idbarang'=>$model->id]); ?>" data-target="#modalvote" data-toggle="modal"><?php echo 'Tambah Ket'; ?></a><br><a href="<?php echo \yii\helpers\Url::to(['/barang/data-keterangan', 'id' => $val->id]); ?>" target="_Blank"><?php echo 'Views'; ?>,</a>
                            </td>





                        </tr>
                    <?php }
                    ?>
                </table>
            </div>
        </div>

    </div>
</div>
<div class="modal remote fade" id="modalvote">
    <div class="modal-dialog">
        <div class="modal-content loader-lg"></div>
    </div>
</div>