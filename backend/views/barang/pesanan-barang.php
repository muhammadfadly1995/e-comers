<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\money\MaskMoney;
/* @var $this yii\web\View */
/* @var $model common\models\Barang */

$modelName = 'Barang';
$this->title = 'Pemesanan Barang | ' ;
$this->params['breadcrumbs'][] = ['label' => 'Barang', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Pemesanan Barang';
?>

<div class="box-header">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
</div>
<div class="box">
    <div class="box-body">
    <ul class="nav nav-tabs">
            <li class=""><?= Html::a('Data Barang', ['/barang']) ?></li>

            <li class=""><?= Html::a('Satuan Barang', ['/satuan-barang']) ?></li>

            <li class=""><?= Html::a('Kategori Barang', ['/kategori-barang']) ?></li>
            <li class=""><?= Html::a('Pesanan Barang', ['/barang/pesanan']) ?></li>
            <li class=""><?= Html::a('Menu Barang', ['/menu-barang']) ?></li>
           
            


        </ul>
        <div class="row">
            <div class="col-md-12">
                <div class="box-header">
                    <h3 class="box-title"><?= 'Data Pesanan Barang' ?></h3>
                </div>
                <table class="table table-bordered table-hover table-responsive table-striped" cellpadding="6" cellspacing="1" style="width:100%" border="1">
                    <thead style="background-color: #7fc6b6;">

                        <tr>
                            <th>No</th>
                            <th>Nama Pelanggan</th>

                            <th>Provinsi</th>
                            <th>Kota/kabupaten</th>
                            
                            <th>Alamat</th>
                            <th>Jenis Pembayaran</th>
                            <th>Action</th>


                        </tr>
                    </thead>

                
                        <tr>
                            <td>1</td>
                            <td>Pelanggan 1</td>

                            <td>Jawa Barat</td>
                            <td>Bandung</td>
                            
                            <td>Jln. Proklamasi No 100</td>
                            <td>COD</td>
                            <td><a href="<?php echo \yii\helpers\Url::to(['/barang/konfirmasi-pemesanan', 'id' => 1]); ?>" target="_Blank" ><?php echo 'Konfirmasi'; ?></a></td>


                        </tr>
                        <tr>
                        <td>2</td>
                            <td>Pelanggan 2</td>

                            <td>Jawa Barat</td>
                            <td>Bandung</td>
                            
                            <td>Jln. Proklamasi No 100</td>
                            <td>COD</td>
                            <td><a href="<?php echo \yii\helpers\Url::to(['/barang/konfirmasi-pemesanan', 'id' => 1]); ?>" target="_Blank" ><?php echo 'Konfirmasi'; ?></a></td>



                        </tr>
                        <tr>
                        <td>3</td>
                            <td>Pelanggan 3</td>

                            <td>Jawa Barat</td>
                            <td>Bandung</td>
                            
                            <td>Jln. Proklamasi No 100</td>
                            <td>COD</td>
                            <td><a href="<?php echo \yii\helpers\Url::to(['/barang/konfirmasi-pemesanan', 'id' => 1]); ?>" target="_Blank" ><?php echo 'Konfirmasi'; ?></a></td>



                        </tr>
                    

                </table>
            </div>
        </div>


     
    </div>
</div>
