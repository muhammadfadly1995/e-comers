<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\money\MaskMoney;
/* @var $this yii\web\View */
/* @var $model common\models\Barang */

$modelName = 'Barang';
$this->title = 'Pemesanan Barang | ' ;
$this->params['breadcrumbs'][] = ['label' => 'Barang', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Pemesanan Barang';
?>

<div class="box-header">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
</div>
<div class="box">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <div class="box-header">
                    <h3 class="box-title"><?= 'Data Barang' ?></h3>
                </div>
                <table class="table table-bordered table-hover table-responsive table-striped" cellpadding="6" cellspacing="1" style="width:100%" border="1">
                    <thead style="background-color: #7fc6b6;">

                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>

                            <th>Harga Barang</th>
                            <th>Warna Barang</th>
                            <th>Jumlah Barang</th>
                            <th>Total</th>
                            


                        </tr>
                    </thead>

                  
                  
                        <tr>
                            <td>1</td>
                            <td>Laptop Lenovo</td>

                            <td>Rp. 30.000</td>
                            <td>Metalik</td>
                            <td>2</td>
                            <td>Rp. 60.000</td>


                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Pakaian Pria</td>

                            <td>Rp. 80.000</td>
                            <td>Merah</td>
                            <td>2</td>
                            <td>Rp. 160.000</td>


                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Jam Tangan AC</td>

                            <td>Rp. 2000.000</td>
                            <td>Hitam Metalik</td>
                            <td>3</td>
                            <td>Rp. 6000.000</td>


                        </tr>
                        <tr>
                      
                        <td colspan="2" border="0"> </td>
                        <td colspan="" border="0"> </td>
                        <td colspan="" border="0"> </td>
                        <td class="text-left"><strong> Total Pembayaran</strong></td>
                        <td class="text-left"><?php echo 'Rp.'. number_format(6220000); ?></td>

                    </tr>

                </table>
            </div>
        </div>


       
    </div>
</div>
