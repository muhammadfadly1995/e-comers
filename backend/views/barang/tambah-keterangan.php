<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use kartik\money\MaskMoney;
?>
<?php
 $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>


<div class="modal-header" style="background-color:#7fc6b6 ">
    <button type="button" class="close" data-dismiss="modal">&times;</button>


</div>
<div class="modal-body">
    <?php
    echo $form->field($modelketerangan, 'keterangan_warna')->textInput() ?>
    
    <?php
    echo $form->field($modelketerangan, 'keterangan_lainya')->textInput() ?>
    <?php
    echo $form->field($modelfoto, 'imageFiles[]')->fileInput(['multiple' => true,'accept' => 'image/*']) ?>

</div>
<div class="modal-footer" style="background-color:#7fc6b6 ">
<?= Html::submitButton('Simpan', ['class' => 'btn btn-primary', 'name' => 'simpan', 'value' => 'true']) ?>
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
<?php ActiveForm::end(); ?>