<?php

use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model common\models\SpesifikasiBarang */

$modelName = 'Spesifikasi Barang';
$this->title = 'Input Spesifikasi Barang';
$this->params['breadcrumbs'][] = ['label' => 'Spesifikasi Barang', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box-header">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3> 
</div>
<div class="box-body">
    <div class="row">
        <div class="col-lg-12">

            <div class="admin-create">

                <div class="spesifikasi-barang-create">

                    <?= $this->render('_form', [
                    'model' => $model,
                    ]) ?>

                </div>


            </div>
        </div>
    </div>
</div>