<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SpesifikasiBarangSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Spesifikasi Barang';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box-header">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    <div class="pull-right">
        <a class="btn btn-primary btn-sm" href="<?= Url::toRoute(['create']) ?>">Tambahkan Data</a>
    </div>
</div>
<div class="box-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="spesifikasi-barang-index">
                                        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                
                <div class="table-responsive">
                                                                <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            [
                            'class' => 'yii\grid\SerialColumn',
                            'header' => 'No.'
                            ],

                                        'id',
            'id_barang',
            'id_user',
            'keterangan',
            'isi_keterangan',

                        ['class' => 'yii\grid\ActionColumn'],
                        ],
                        ]); ?>
                                        
                </div>
            </div>

        </div>
    </div>
</div>