<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SpesifikasiBarang */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="spesifikasi-barang-form">

    <?php $form = ActiveForm::begin([
    'layout' => 'horizontal',
    ]); ?>

    <div class="row">
        <div class="col-lg-6">
                <?= $form->field($model, 'id_barang')->textInput() ?>

    <?= $form->field($model, 'id_user')->textInput() ?>

    <?= $form->field($model, 'keterangan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'isi_keterangan')->textInput(['maxlength' => true]) ?>

        </div>
    </div>

    <div class="col-lg-6">
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <a href="./index" class="btn btn-default">Kembali</a>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>