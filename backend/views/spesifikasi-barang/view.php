<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\SpesifikasiBarang */

$modelName = 'Spesifikasi Barang';
$this->title = 'Spesifikasi Barang | ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Spesifikasi Barang', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box-header">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
</div>
<div class="box-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="spesifikasi-barang-view">

                <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                            'id',
            'id_barang',
            'id_user',
            'keterangan',
            'isi_keterangan',
                ],
                ]) ?>
                <a href="./index" class="btn btn-default">Kembali</a>
            </div>


        </div>
    </div>
</div>