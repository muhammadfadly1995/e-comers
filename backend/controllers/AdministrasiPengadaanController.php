<?php

namespace backend\controllers;

use Yii;
use common\models\AdministrasiPengadaan;
use common\models\AdministrasiPengadaanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AdministrasiPengadaanController implements the CRUD actions for AdministrasiPengadaan model.
 */
class AdministrasiPengadaanController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AdministrasiPengadaan models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new AdministrasiPengadaanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AdministrasiPengadaan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AdministrasiPengadaan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new AdministrasiPengadaan();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['tambah-data', 'id' => $model->id_cara_pengadaan]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    public function actionTambahData($id) {
        $datapengadaan = \common\models\CaraPengadaan::find()->where(['id' => $id])->one();
        $dataadministrasi = AdministrasiPengadaan::find()->where(['id_cara_pengadaan' => $id])->all();
        $model = new AdministrasiPengadaan();

        if ($model->load(Yii::$app->request->post())) {
            $model->id_cara_pengadaan=$id;
            $model->save(false);
            return $this->redirect(['tambah-data', 'id' => $id]);
        } else {
            return $this->render('create1', [
                        'dataadministrasi' => $dataadministrasi,
                        'datapengadaan' => $datapengadaan,
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AdministrasiPengadaan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AdministrasiPengadaan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AdministrasiPengadaan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AdministrasiPengadaan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = AdministrasiPengadaan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
