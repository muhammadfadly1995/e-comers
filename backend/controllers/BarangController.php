<?php

namespace backend\controllers;

use Yii;
use common\models\Barang;
use common\models\BarangSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\Url;
use yii\imagine\Image;

/**
 * BarangController implements the CRUD actions for Barang model.
 */
class BarangController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Barang models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BarangSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Barang model.
     * @param integer $id
     * @return mixed
     */

    public function actionDataKeterangan($id){
        $dataketerangan=\common\models\SpesifikasiBarang::find()->where(['id_barang'=>$id])->all();
        $dataukuran=\common\models\UkuranBarang::find()->where(['id'=>$id])->one();
        return $this->render('keterangan-barang', [
           'dataketerangan'=>$dataketerangan,
           'dataukuran'=>$dataukuran,
        ]);
    }
    public function actionKonfirmasiPemesanan($id){
        return $this->render('konfirmasi-barang', [
           
            ]);
    }
    public function actionPesanan(){

        return $this->render('pesanan-barang', [
           
        ]);
    }
    public function actionTambahKeterangan($id, $idbarang)
    {
        $modelfoto = new \common\models\FotoBarang();
        $modelketerangan = new \common\models\SpesifikasiBarang();
        if (Yii::$app->request->post('simpan') == 'true') {
            $modelketerangan->load(Yii::$app->request->post());
            $modelfoto->load(Yii::$app->request->post());
            $modelketerangan->id_barang = $id;
            $modelketerangan->save(false);
           
            $modelfoto->imageFiles = UploadedFile::getInstances($modelfoto, 'imageFiles');
            //  print_r($modelfoto);die();

            foreach ($modelfoto->imageFiles as $key => $val) {
                $modelfoto = new \common\models\FotoBarang();

                $imgName = rand(1, 1000) . $val->baseName . '.' . $val->getExtension();
                $modelfoto->foto = $imgName;
                $modelfoto->id_keterangan = $modelketerangan->id;
                $val->saveAs('img/fotobarang/' . $imgName);
                $modelfoto->save(false);
            }

            return $this->redirect(['view', 'id' => $idbarang]);
        }


        return $this->renderAjax('tambah-keterangan', [
            'modelfoto' => $modelfoto,
            'modelketerangan' => $modelketerangan,
        ]);
    }

    public function actionView($id)
    {
        $modelukuran = new \common\models\UkuranBarang();
        $modelfoto = new \common\models\FotoBarang();
        $modelketeranganukuran = new \common\models\SpesifikasiBarang();
        $databarang = Barang::find()->where(['id' => $id])->one();
        $dataukuran = \common\models\UkuranBarang::find()->where(['id_barang' => $id])->all();
        if (Yii::$app->request->post('simpan') == 'true') {
            $modelketeranganukuran->load(Yii::$app->request->post());
            $modelukuran->load(Yii::$app->request->post());
          //  print_r($modelukuran->harga_beli);die();
            $modelfoto->load(Yii::$app->request->post());
            $modelukuran->id_barang = $id;
            $modelukuran->save(false);
            $modelketeranganukuran->id_barang = $modelukuran->id;
            $modelketeranganukuran->save(false);
            $modelfoto->imageFiles = UploadedFile::getInstances($modelfoto, 'imageFiles');
            //  print_r($modelfoto);die();

            foreach ($modelfoto->imageFiles as $key => $val) {
                $modelfoto = new \common\models\FotoBarang();

                $imgName = rand(1, 1000) . $val->baseName . '.' . $val->getExtension();
                $modelfoto->foto = $imgName;
                $modelfoto->id_keterangan = $modelketeranganukuran->id;
                $val->saveAs('img/fotobarang/' . $imgName);
                $modelfoto->save(false);
            }

            $databarang->jumlah_barang = $modelukuran->jumlah_barang + $databarang->jumlah_barang;
            $databarang->save(false);
            return $this->redirect(['view', 'id' => $id]);
        }

        return $this->render('view', [
            'modelketeranganukuran' => $modelketeranganukuran,
            'model' => $this->findModel($id),
            'modelfoto' => $modelfoto,
            'modelukuran' => $modelukuran,
            'dataukuran' => $dataukuran,
        ]);
    }

    /**
     * Creates a new Barang model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $modelspesifikasibarang = new \common\models\SpesifikasiBarang();

        $model = new Barang();
        $model->id_user = \yii::$app->user->identity->id;

        if ($model->load(Yii::$app->request->post())) {
            $model->imageFiles = UploadedFile::getInstances($model, 'imageFiles');
            //  print_r($modelfoto);die();

            foreach ($model->imageFiles as $key => $val) {
                

                $imgName = rand(1, 1000) . $val->baseName . '.' . $val->getExtension();
                $model->latar_foto = $imgName;
               
                $val->saveAs('img/fotolatar/' . $imgName);
               
            }

            $model->jumlah_barang = 0;
            $model->save(false);

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'modelspesifikasibarang' => $modelspesifikasibarang,
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Barang model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Barang model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Barang model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Barang the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Barang::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
