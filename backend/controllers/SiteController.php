<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Admin;
use common\models\Himpunan;
use common\models\LoginForm;
use common\models\SignupForm;
use common\models\User;
use yii\web\UploadedFile;
use yii\imagine\Image;

/**
 * Site controller
 */
class SiteController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'signup', 'error','tambah-baru','master-aset','inventarisasi','stock-opname'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'profile', 'profile-photo', 'change-password','rubah'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            //            'error' => [
            //                'class' => 'yii\web\ErrorAction',
            //            ],
        ];
    }

    public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            if (Yii::$app->user->id) {
                $this->layout = 'main';
            } else {
                $this->layout = 'login';
            }
            return $this->render('error', ['exception' => $exception]);
        }
    }

    public function actionRubah(){
        $datauser=  User::find()->all();
        foreach($datauser as $key=>$val){
            if($val->jabatan){
                $jabatan=  \common\models\Jabatan::find()->where(['id'=>$val->jabatan])->one();
                $val->jabatan=$jabatan->nama;
                $val->save(false);
            }
            if($val->pangkat){
                $pangkat=  \common\models\Pangkat::find()->where(['id'=>$val->pangkat])->one();
                $val->pangkat=$pangkat->nama;
                $val->save(false);
                
            }
            if($val->kesatuan){
                $kesatuan=  \common\models\Kesatuan::find()->where(['id'=>$val->kesatuan])->one();
                $val->kesatuan=$kesatuan->nama;
                $val->save(false);
            }
        }
    }
    
    public function actionChangePassword()
    {
        $user = \common\models\User::findOne(Yii::$app->user->id);
        $user->scenario = 'change_password';

        // var_dump($user);die;
        $loadedPost = $user->load(Yii::$app->request->post());

        if ($loadedPost && $user->validate()) {
            $user->password = $user->newPassword;

            if ($user->save(false)) {
                Yii::$app->session->setFlash('success', 'Password Berhasil Diperbarui');
                return $this->refresh();
            }
        }

        return $this->render("change-password", [
            'user' => $user,
        ]);
    }

    public function actionProfile()
    {
        $model =  User::findOne(Yii::$app->user->id);

        if ($model->load(Yii::$app->request->post())) {
            $model->input_photo = UploadedFile::getInstance($model, 'input_photo');

            if ($model->input_photo) {
                $photo = time() . $model->username . $model->input_photo->baseName . '.' . $model->input_photo->extension;
                $model->photo = $photo;
                if ($model->save()) {
                    if ($model->input_photo->saveAs(Yii::getAlias('@home/uploads/profile_photo/') . $photo)) {
                        $val = Image::thumbnail(Yii::getAlias('@home/uploads/profile_photo/') . $photo, 320, 320)
                            ->save(Yii::getAlias('@home/uploads/profile_photo/') . 'thumb-' . $photo, ['quality' => 50]);
                        if ($val) {
                            return $this->redirect('profile');
                        }
                    }
                }
            } else {
                if ($model->save()) {
                    return $this->redirect('profile');
                }
            }
        }
        return $this->render('profile', [
            'model' => $model,
        ]);
    }
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionMasterAset()
    {
         $model=new \common\models\KartuKontrol();
        return $this->render('master-aset', [
            'model'=>$model,
           // 'daftarkartukontrol'=>$daftarkartukontrol,
                    
        ]);
    }
     public function actionInventarisasi()
    {
         $model=new \common\models\KartuKontrol();
        return $this->render('master-aset', [
            'model'=>$model,
           // 'daftarkartukontrol'=>$daftarkartukontrol,
                    
        ]);
    }
     public function actionStockOpname()
    {
         $model=new \common\models\KartuKontrol();
        return $this->render('master-aset', [
            'model'=>$model,
           // 'daftarkartukontrol'=>$daftarkartukontrol,
                    
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin($username = null)
    {
        $this->layout = 'login';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        $model->bilangan_pertama=rand(10,99);
        $model->bilangan_kedua=rand(1,10);
        $model->username = $username;
         if ($model->load(Yii::$app->request->post())) {
            $datahasil=$model->bilangan_pertama+$model->bilangan_kedua;
           // print_r($datahasil);die();
           if($model->bilangan_hasil == $datahasil){
               $model->login();
            return $this->goBack();
           }else{
                return $this->redirect('login');
           }
            
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionSignup()
    {
        $this->layout = 'login';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            $model->role = 5;
            if ($user = $model->signup()) {
                $user = User::findOne(['username' => $model->username]);
                $modell = new Himpunan();
                $modell->id_himpunan = $user->id;
                $modell->nama_himpunan = $model->nama;
                $modell->id_pembina = $model->id_pembina;
                $modell->ket_himpunan = $model->ket_himpunan;

                if ($modell->save()) {
                    return $this->redirect(['login', 'username' => $model->username]);
                }
                return $this->redirect(['login', 'username' => $model->username]);
            }
        }
        return $this->render('signup', [
            'model' => $model,
        ]);
    }
public function actionTambahBaru(){
        {
       
        $model = new \common\models\SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            $model->role='vendor';
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->renderAjax('daftar', [
            'model' => $model,
        ]);
    }
}
    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionProfilePhoto($inline = false)
    {
        $user = Yii::$app->user->identity;

        if ($user) {
            $path = \Yii::getAlias('@home') . '/uploads/profile_photo/thumb-';
            $file_download = $path . $user->photo;
            $fake_filename = $user->photo;
            $file_default = $path . 'default.png';
            $fake_filename_default = 'default.png';
            $response = Yii::$app->getResponse();

            if ($file_download && $fake_filename) {
                $response->sendFile($file_download, $fake_filename, ['inline' => $inline]);
            } else {
                $response->sendFile($file_default, $fake_filename_default, ['inline' => $inline]);
            }
        } else {
            throw new \yii\web\NotFoundHttpException('File not found');
        }
    }
}
