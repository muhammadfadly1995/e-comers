<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'name' => $this->string()->notNull(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'photo' => $this->string()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'role' => $this->string()->notNull(),
        ], $tableOptions);

        $this->insert('user', array(
            'id' => '1',
            'username' => 'admin',
            'name' => 'Admin',
            'auth_key' => 'UlY8F83Uf9FZg0i8DDu4dFcKoP4DWUXC',
            'password_hash' => '$2y$13$LHvPq4ZL2zvR5N9fXeHsAOHOTGChnqt4.DWRpcotNXnKJFKt/7Yl6',
            'password_reset_token' => '',
            'email' => 'harisabintang115@gmail.com',
            'photo' => 'admin.png',
            'status' => 10,
            'created_at' => 0,
            'updated_at' => 0,
            'role' => 'admin',
        ));
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
