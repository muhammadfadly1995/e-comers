<?php
return [
    'adminEmail' => 'admin@sevensys.id',
    'supportEmail' => 'support@sevensys.id',
    'user.passwordResetTokenExpire' => 3600,
];
