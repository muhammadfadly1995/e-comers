<?php

return [
    'name' => '7sys Mobile',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'modules' => [
        'gridview' => [
            'class' => 'kartik\grid\Module'
        ],
        'redactor' => [
            'class' => 'yii\redactor\RedactorModule',
            'uploadDir' => '@webroot/uploads',
            'uploadUrl' => '@web/uploads',
            'imageAllowExtensions' => ['jpg', 'png', 'gif']
        ], 
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        // 'authManager' => [
        //     'class' => 'dektrium\rbac\components\DbManager',
        // ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'dateFormat' => 'dd-MM-yyyy',
            'decimalSeparator' => ',',
            'thousandSeparator' => '.',
            'currencyCode' => 'Rp. ',
            'nullDisplay' => '',
            'locale' => 'id_ID.utf8',
//            'locale' => 'id_ID',
            'timeZone' => 'Asia/Jakarta',
             //'timeFormat' => 'php:H:i:s',
        ],
    ],
    'bootstrap' => ['log'],
];
