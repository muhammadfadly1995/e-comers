<?php

namespace common\models;
use yii\web\UploadedFile;
use Yii;

/**
 * This is the model class for table "foto_barang".
 *
 * @property integer $id
 * @property string $id_barang
 * @property string $foto
 */
class FotoBarang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'foto_barang';
    }
    public $imageFiles;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_keterangan', 'foto'], 'required'],
            [['foto'], 'string'],
            [['id_keterangan'], 'string', 'max' => 255],
            [['imageFiles'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg', 'maxFiles' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_barang' => 'Id Barang',
            'foto' => 'Foto',
        ];
    }
}
