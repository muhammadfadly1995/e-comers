<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ukuran_barang".
 *
 * @property integer $id
 * @property integer $id_barang
 * @property integer $harga_beli
 * @property string $ukuran
 * @property integer $harga
 * @property integer $jumlah_barang
 * @property string $isi_keterangan
 */
class UkuranBarang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ukuran_barang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_barang', 'harga_beli', 'ukuran', 'harga', 'jumlah_barang'], 'required'],
            [['id_barang', 'harga_beli', 'harga', 'jumlah_barang'], 'integer'],
            [['isi_keterangan'], 'string'],
            [['ukuran'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_barang' => 'Id Barang',
            'harga_beli' => 'Harga Beli',
            'ukuran' => 'Ukuran',
            'harga' => 'Harga',
            'jumlah_barang' => 'Jumlah Barang',
            'isi_keterangan' => 'Isi Keterangan',
        ];
    }
}
