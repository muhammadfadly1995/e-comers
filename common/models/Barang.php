<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "barang".
 *
 * @property integer $id
 * @property string $nama_barang
 * @property integer $harga_beli
 * @property integer $harga_jual
 * @property string $satuan
 * @property integer $jumlah_barang
 * @property string $merk_barang
 * @property string $spesifikasi_barang
 * @property integer $id_user
 */
class Barang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $imageFiles;
    public static function tableName()
    {
        return 'barang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_barang', 'satuan', 'jumlah_barang', 'merk_barang', 'spesifikasi_barang', 'id_user','id_kategori','menu_barang','latar_foto'], 'required'],
            [[ 'jumlah_barang', 'id_user','id_kategori'], 'integer'],
          
            [['nama_barang', 'satuan', 'merk_barang','menu_barang','latar_foto'], 'string', 'max' => 255],
            [['imageFiles'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg', 'maxFiles' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_barang' => 'Nama Barang',
            'menu_barang'=>'Menu Barang',
            'satuan' => 'Satuan',
            'jumlah_barang' => 'Jumlah Barang',
            'merk_barang' => 'Merk Barang',
           'id_kategori'=>'Kategori',
            'id_user' => 'Id User',
        ];
    }
}
