<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "spesifikasi_barang".
 *
 * @property integer $id
 * @property integer $id_barang
 * @property integer $id_user
 * @property string $keterangan
 * @property string $isi_keterangan
 */
class SpesifikasiBarang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'spesifikasi_barang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_barang', 'id_keterangan', 'keterangan_warna', 'keterangan_lainya'], 'required'],
            [['id_barang', 'id_keterangan'], 'integer'],
            [['keterangan_warna', 'keterangan_lainya'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_barang' => 'Id Barang',
            'id_keterangan' => 'id keterangan',
            'keterangan_warna' => 'Keterangan Warna',
            'keterangan_lainya' => 'Keterangan Lainya',
        ];
    }
}
