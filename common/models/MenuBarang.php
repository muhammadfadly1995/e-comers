<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "menu_barang".
 *
 * @property integer $id
 * @property string $keterangan
 */
class MenuBarang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu_barang';
    }
    public $imageFiles;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['keterangan','foto'], 'required'],
            [['keterangan','foto'], 'string', 'max' => 255],
            [['imageFiles'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg', 'maxFiles' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'keterangan' => 'Keterangan',
            'foto'=>'Foto',
        ];
    }
}
