-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 22, 2021 at 06:06 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecomers`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id` int(11) NOT NULL,
  `nama_barang` varchar(255) NOT NULL,
  `satuan` varchar(255) NOT NULL,
  `jumlah_barang` int(11) NOT NULL,
  `merk_barang` varchar(255) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `menu_barang` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id`, `nama_barang`, `satuan`, `jumlah_barang`, `merk_barang`, `id_user`, `id_kategori`, `menu_barang`) VALUES
(2, 'Laptop', '3', 50, 'Lenovo', 1, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `foto_barang`
--

CREATE TABLE `foto_barang` (
  `id` int(11) NOT NULL,
  `id_keterangan` varchar(255) NOT NULL,
  `foto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `foto_barang`
--

INSERT INTO `foto_barang` (`id`, `id_keterangan`, `foto`) VALUES
(7, '6', '144BUKTI PEMBAYARAN.jpg'),
(8, '6', '264Ktp.jpeg'),
(9, '7', '690Ktp.jpeg'),
(10, '7', '13Ktp2.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_barang`
--

CREATE TABLE `kategori_barang` (
  `id` int(11) NOT NULL,
  `keterangan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_barang`
--

INSERT INTO `kategori_barang` (`id`, `keterangan`) VALUES
(1, 'Elektronik');

-- --------------------------------------------------------

--
-- Table structure for table `satuan_barang`
--

CREATE TABLE `satuan_barang` (
  `id` int(11) NOT NULL,
  `keterangan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `satuan_barang`
--

INSERT INTO `satuan_barang` (`id`, `keterangan`) VALUES
(1, 'BOX'),
(2, 'DUS'),
(3, 'PCS');

-- --------------------------------------------------------

--
-- Table structure for table `spesifikasi_barang`
--

CREATE TABLE `spesifikasi_barang` (
  `id` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `id_keterangan` int(11) NOT NULL,
  `keterangan_warna` varchar(255) NOT NULL,
  `keterangan_lainya` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `spesifikasi_barang`
--

INSERT INTO `spesifikasi_barang` (`id`, `id_barang`, `id_keterangan`, `keterangan_warna`, `keterangan_lainya`) VALUES
(6, 6, 0, 'Merah', 'Keterangan Lainya'),
(7, 6, 0, 'Hitam', '-');

-- --------------------------------------------------------

--
-- Table structure for table `ukuran_barang`
--

CREATE TABLE `ukuran_barang` (
  `id` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `harga_beli` int(11) NOT NULL,
  `ukuran` varchar(255) NOT NULL,
  `harga` int(11) NOT NULL,
  `jumlah_barang` int(11) NOT NULL,
  `isi_keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ukuran_barang`
--

INSERT INTO `ukuran_barang` (`id`, `id_barang`, `harga_beli`, `ukuran`, `harga`, `jumlah_barang`, `isi_keterangan`) VALUES
(6, 2, 20000, 'M', 21000, 10, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `name`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `photo`, `status`, `created_at`, `updated_at`, `role`) VALUES
(1, 'admin', 'Admin', 'UlY8F83Uf9FZg0i8DDu4dFcKoP4DWUXC', '$2y$13$LHvPq4ZL2zvR5N9fXeHsAOHOTGChnqt4.DWRpcotNXnKJFKt/7Yl6', '', 'admin@gmail.com', '1613320326admin97767959.jpg', 10, 0, 1613320326, '1'),
(4, 'admin smak', 'Admin Simak', '-Tsy2cpgX6zpi0lKaqIrAXNl9Daonpj2', '$2y$13$5Me8qDbF7PvIJqZyKhZqkOUD3IssbbS6.K6Zrs.rk/4zeMjo946nW', NULL, 'simak@gmail.com', '', 10, 1611559678, 1611559678, '3'),
(5, 'admin pengadaan', 'admin Pengadaan', '37l85rtb0qm1uwrm7PnsYzsKCKyojKII', '$2y$13$pNsKmv8dwfHw/LxP6jBLUOfucJDuOXoP6.VInOt1Ek2tTv9X9Wgsq', NULL, 'pengadaan@gmail.com', '', 10, 1611559697, 1611559697, '2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `foto_barang`
--
ALTER TABLE `foto_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_barang`
--
ALTER TABLE `kategori_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `satuan_barang`
--
ALTER TABLE `satuan_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spesifikasi_barang`
--
ALTER TABLE `spesifikasi_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ukuran_barang`
--
ALTER TABLE `ukuran_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `foto_barang`
--
ALTER TABLE `foto_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `kategori_barang`
--
ALTER TABLE `kategori_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `satuan_barang`
--
ALTER TABLE `satuan_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `spesifikasi_barang`
--
ALTER TABLE `spesifikasi_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `ukuran_barang`
--
ALTER TABLE `ukuran_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
