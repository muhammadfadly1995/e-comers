<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>E-COMMERCE | UMKM GO DIGITAL</title>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <link rel="icon" href="<?= Yii::getAlias('@web/img/fav-icon.png') ?>" type="image/x-icon" />


    <!-- Icon css link -->
    <link href="<?= Yii::getAlias('@web/css/font-awesome.min.css') ?>" rel="stylesheet">
    <link href="<?= Yii::getAlias('@web/vendors/line-icon/css/simple-line-icons.css') ?>" rel="stylesheet">
    <link href="<?= Yii::getAlias('@web/vendors/elegant-icon/style.css') ?>" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="<?= Yii::getAlias('@web/css/bootstrap.min.css') ?>" rel="stylesheet">

    <!-- Rev slider css -->
    <link href="<?= Yii::getAlias('@web/vendors/revolution/css/settings.css') ?>" rel="stylesheet">
    <link href="<?= Yii::getAlias('@web/vendors/revolution/css/layers.css') ?>" rel="stylesheet">
    <link href="<?= Yii::getAlias('@web/vendors/revolution/css/navigation.css') ?>" rel="stylesheet">

    <!-- Extra plugin css -->
    <link href="<?= Yii::getAlias('@web/vendors/owl-carousel/owl.carousel.min.css') ?>" rel="stylesheet">
    <link href="<?= Yii::getAlias('@web/vendors/bootstrap-selector/css/bootstrap-select.min.css') ?>" rel="stylesheet">

    <link href="<?= Yii::getAlias('@web/css/style.css') ?>" rel="stylesheet">
    <link href="<?= Yii::getAlias('@web/css/responsive.css') ?>" rel="stylesheet">
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
        <script src="js/respond.min.js"></script>
        <![endif]-->
</head>

<body>
    <header class="shop_header_area carousel_menu_area">
        <div class="carousel_top_header row m0">
            <div class="container">
                <div class="carousel_top_h_inner">
                    <div class="float-md-left">
                        <div class="top_header_left">
                            <div class="selector">
                                <select class="language_drop" name="countries" id="countries" style="width:300px;">
                                    <option value='yt' data-image="<?= Yii::getAlias('@web/img/icon/flag-1.png') ?>" data-imagecss="flag yt" data-title="English">English</option>
                                    <option value='yu' data-image="<?= Yii::getAlias('@web/img/icon/flag-1.png') ?>" data-imagecss="flag yu" data-title="Bangladesh">Bangla</option>
                                    <option value='yt' data-image="<?= Yii::getAlias('@web/img/icon/flag-1.png') ?>" data-imagecss="flag yt" data-title="English">English</option>
                                    <option value='yu' data-image="<?= Yii::getAlias('@web/img/icon/flag-1.png') ?>" data-imagecss="flag yu" data-title="Bangladesh">Bangla</option>
                                </select>
                            </div>
                            <select class="selectpicker usd_select">
                                <option>USD</option>
                                <option>$</option>
                                <option>$</option>
                            </select>
                        </div>
                    </div>
                    <div class="float-md-right">
                        <div class="top_header_middle">
                            <a href="#"><i class="fa fa-phone"></i> Call Us: <span>-</span></a>
                            <a href="#"><i class="fa fa-envelope"></i> Email: <span>- </span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel_menu_inner">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <a class="navbar-brand" href="index.php"><img src="<?= Yii::getAlias('@web/img/dabula.png') ?>" alt="" class="img-fluid"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>

                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>
                            <li class="nav-item dropdown submenu">
                                <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Shop <i class="fa fa-angle-down" aria-hidden="true"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="nav-item"><a class="nav-link" href="offer.php">Men</a></li>
                                    <li class="nav-item"><a class="nav-link" href="offer.php">Women</a></li>
                                    <li class="nav-item"><a class="nav-link" href="offer.php">Child</a></li>
                                </ul>
                            </li>
                            <li class="nav-item"><a class="nav-link" href="#">Offer</a></li>
                            <li class="nav-item"><a class="nav-link" href="register-more.php">Register</a></li>
                            <li class="nav-item"><a class="nav-link" href="login.php">Login</a></li>
                            <li class="nav-item"><a class="nav-link" href="contact.php">Contact</a></li>
                        </ul>
                        <ul class="navbar-nav justify-content-end">
                            <li class="search_icon"><a href="#"><i class="icon-magnifier icons"></i></a></li>
                            <li class="user_icon"><a href="#"><i class="icon-user icons"></i></a></li>
                            <li class="cart_cart"><a href="empty-cart.php"><i class="icon-handbag icons"></i></a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </header>

    <?= $content ?>

    <footer class="footer_area">
        <div class="container">
            <div class="footer_widgets">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-6">
                        <aside class="f_widget f_about_widget">
                            <img src="<?= Yii::getAlias('@web/img/logo.png') ?>img/logo.png" alt="">
                            <p>Persuit is a Premium PSD Template. Best choice for your online store. Let purchase it to enjoy now</p>
                            <h6>Social:</h6>
                            <ul>
                                <li><a href="#"><i class="social_facebook"></i></a></li>
                                <li><a href="#"><i class="social_twitter"></i></a></li>
                                <li><a href="#"><i class="social_pinterest"></i></a></li>
                                <li><a href="#"><i class="social_instagram"></i></a></li>
                                <li><a href="#"><i class="social_youtube"></i></a></li>
                            </ul>
                        </aside>
                    </div>
                    <div class="col-lg-2 col-md-4 col-6">
                        <aside class="f_widget link_widget f_info_widget">
                            <div class="f_w_title">
                                <h3>Information</h3>
                            </div>
                            <ul>
                                <li><a href="#">About us</a></li>
                                <li><a href="#">Delivery information</a></li>
                                <li><a href="#">Terms & Conditions</a></li>
                                <li><a href="#">Help Center</a></li>
                                <li><a href="#">Returns & Refunds</a></li>
                            </ul>
                        </aside>
                    </div>
                    <div class="col-lg-2 col-md-4 col-6">
                        <aside class="f_widget link_widget f_service_widget">
                            <div class="f_w_title">
                                <h3>Customer Service</h3>
                            </div>
                            <ul>
                                <li><a href="#">My account</a></li>
                                <li><a href="#">Ordr History</a></li>
                                <li><a href="#">Wish List</a></li>
                                <li><a href="#">Newsletter</a></li>
                                <li><a href="#">Contact Us</a></li>
                            </ul>
                        </aside>
                    </div>
                    <div class="col-lg-2 col-md-4 col-6">
                        <aside class="f_widget link_widget f_extra_widget">
                            <div class="f_w_title">
                                <h3>Extras</h3>
                            </div>
                            <ul>
                                <li><a href="#">Brands</a></li>
                                <li><a href="#">Gift Vouchers</a></li>
                                <li><a href="#">Affiliates</a></li>
                                <li><a href="#">Specials</a></li>
                            </ul>
                        </aside>
                    </div>
                    <div class="col-lg-2 col-md-4 col-6">
                        <aside class="f_widget link_widget f_account_widget">
                            <div class="f_w_title">
                                <h3>My Account</h3>
                            </div>
                            <ul>
                                <li><a href="#">My account</a></li>
                                <li><a href="#">Ordr History</a></li>
                                <li><a href="#">Wish List</a></li>
                                <li><a href="#">Newsletter</a></li>
                            </ul>
                        </aside>
                    </div>
                </div>
            </div>
            <div class="footer_copyright">
                <h5>© <script>
                        document.write(new Date().getFullYear());
                    </script> <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright &copy;<script>
                        document.write(new Date().getFullYear());
                    </script> All rights reserved | <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://akatrust.com/" target="_blank">AKATRUST</a>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                </h5>
            </div>
        </div>
    </footer>
    <div class="modal" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Modal Heading</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    Modal body..
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>





    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?= Yii::getAlias('@web/js/jquery-3.2.1.min.js') ?>"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?= Yii::getAlias('@web/js/popper.min.js') ?>"></script>
    <script src="<?= Yii::getAlias('@web/js/bootstrap.min.js') ?>"></script>
    <!-- Rev slider js -->
    <script src="<?= Yii::getAlias('@web/vendors/revolution/js/jquery.themepunch.tools.min.js') ?>"></script>
    <script src="<?= Yii::getAlias('@web/vendors/revolution/js/jquery.themepunch.revolution.min.js') ?>"></script>
    <script src="<?= Yii::getAlias('@web/vendors/revolution/js/extensions/revolution.extension.actions.min.js') ?>"></script>
    <script src="<?= Yii::getAlias('@web/vendors/revolution/js/extensions/revolution.extension.video.min.js') ?>"></script>
    <script src="<?= Yii::getAlias('@web/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js') ?>"></script>
    <script src="<?= Yii::getAlias('@web/vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js') ?>"></script>
    <script src="<?= Yii::getAlias('@web/vendors/revolution/js/extensions/revolution.extension.navigation.min.js') ?>"></script>
    <script src="<?= Yii::getAlias('@web/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js') ?>"></script>
    <!-- Extra plugin css -->
    <script src="<?= Yii::getAlias('@web/vendors/counterup/jquery.waypoints.min.js') ?>"></script>
    <script src="<?= Yii::getAlias('@web/vendors/counterup/jquery.counterup.min.js') ?>"></script>
    <script src="<?= Yii::getAlias('@web/vendors/owl-carousel/owl.carousel.min.js') ?>"></script>
    <script src="<?= Yii::getAlias('@web/vendors/bootstrap-selector/js/bootstrap-select.min.js') ?>"></script>
    <script src="<?= Yii::getAlias('@web/vendors/image-dropdown/jquery.dd.min.js') ?>"></script>
    <script src="<?= Yii::getAlias('@web/js/smoothscroll.js') ?>"></script>
    <script src="<?= Yii::getAlias('@web/vendors/isotope/imagesloaded.pkgd.min.js') ?>"></script>
    <script src="<?= Yii::getAlias('@web/vendors/isotope/isotope.pkgd.min.js') ?>"></script>
    <script src="<?= Yii::getAlias('@web/vendors/magnify-popup/jquery.magnific-popup.min.js') ?>"></script>
    <script src="<?= Yii::getAlias('@web/vendors/vertical-slider/js/jQuery.verticalCarousel.js') ?>"></script>
    <script src="<?= Yii::getAlias('@web/vendors/jquery-ui/jquery-ui.js') ?>"></script>

    <script src="<?= Yii::getAlias('@web/js/theme.js') ?>"></script>
    <script>
        function myAlertTop() {
            $(".myAlert-top").show();
            setTimeout(function() {
                $(".myAlert-top").hide();
            }, 2000);
        }

        function myAlertBottom() {
            $(".myAlert-bottom").show();
            setTimeout(function() {
                $(".myAlert-bottom").hide();
            }, 2000);
        }
    </script>

    <!-- <script>
        $(window).on('load', function() {
            $('#myModal').modal('show');
        });
    </script> -->

</body>

</html>
<?php $this->endPage() ?>