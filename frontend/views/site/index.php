<?php

use common\models\Barang;
use common\models\SpesifikasiBarang;
use common\models\UkuranBarang;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */

$this->title = Yii::$app->name;
?>
<!--================Home Carousel Area =================-->
<section class="home_carousel_area">
    <div class="home_carousel_slider owl-carousel">
        <?php foreach ($datafototerbaru as $keyfoto => $valfoto) {
            $keterangan = SpesifikasiBarang::find()->where(['id' => $valfoto->id_keterangan])->one();
            $ukurangbarang = UkuranBarang::find()->where(['id' => $keterangan->id_barang])->one();
            $databarang = Barang::find()->where(['id' => $ukurangbarang->id_barang])->one();
        ?>
            <div class="item">
                <div class="h_carousel_item">
                    <img src="<?php echo Url::to('@web/dashboard/img/fotobarang/' . $valfoto->foto); ?>" class="img-fluid" alt="">
                    <div class="carousel_hover">
                        <h3><?= $databarang->nama_barang ?></h3>

                        <h5>Including:</h5>
                        <p><?= $keterangan->keterangan_lainya ?></p>
                        <a class="discover_btn" href="offer.php">discover now</a>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</section>
<!--================End Home Carousel Area =================-->

<!--================Special Offer Area =================-->
<section class="special_offer_area">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="special_offer_item">
                    <img class="img-fluid" src="img/feature-add/special-offer-1.jpg" alt="">
                    <div class="hover_text">
                        <h4>Special Offer</h4>
                        <h5>Young Couple</h5>
                        <a class="shop_now_btn" href="offer.php">Shop Now</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="special_offer_item2">
                    <img class="img-fluid" src="img/feature-add/special-offer-2.jpg" alt="">
                    <div class="hover_text">
                        <h5>girls bag</h5>
                        <a class="shop_now_btn" href="offer.php">Shop Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================End Special Offer Area =================-->

<!--================Latest Product isotope Area =================-->
<section class="fillter_latest_product">
    <div class="container">
        <div class="single_c_title">
            <h2>Our Product</h2>
        </div>
        <div class="fillter_l_p_inner">
            <!-- <ul class="fillter_l_p">
                        <li class="active" data-filter="*"><a href="#">men's</a></li>
                        <li data-filter=".woman"><a href="#">Woman</a></li>
                        <li data-filter=".acc"><a href="#">Accessories</a></li>
                        <li data-filter=".shoes"><a href="#">Shoes</a></li>
                        <li data-filter=".bags"><a href="#">Bags</a></li>
                    </ul> -->
            <div class="row">
                <?php foreach ($databarang1 as $keybarang1 => $valbarang1) {

                    $imgPath = Yii::getAlias('@web/dashboard/img/fotolatar/');
                    $img = is_file(Yii::getAlias('@webroot/dashboard/img/fotolatar/') . $valbarang1->latar_foto)
                        ? $imgPath . $valbarang1->latar_foto
                        : $imgPath . $valbarang1->latar_foto;
                ?>
                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="l_product_item">
                            <div class="l_p_img">
                                <img class="img-fluid" src="<?= $img ?>" alt="Foto Latar Barang">
                            </div>
                            <div class="l_p_text">
                                <ul>
                                    <li class="p_icon"><a href="#"><i class="icon_piechart"></i></a></li>
                                    <li><a class="add_cart_btn" href="product-detail.php">Add To Cart</a></li>
                                    <li class="p_icon"><a href="#"><i class="icon_heart_alt"></i></a></li>
                                </ul>
                                <h4><?= $valbarang1->nama_barang ?></h4>
                                <!-- <h5><del>$45.50</del>  $40</h5> -->
                            </div>
                        </div>
                    </div>

                <?php } ?>







            </div>
        </div>
    </div>
</section>
<!--================End Latest Product isotope Area =================-->

<!--================Product_listing Area =================-->
<section class="feature_product_area">
            <div class="container">
                <div class="f_p_inner">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="f_product_left">
                                <div class="s_m_title">
                                    <h2>Featured Products</h2>
                                </div>
                                <div class="f_product_inner">
                                    <div class="media">
                                        <div class="d-flex">
                                            <a href="product-detail.php">
                                               <img src="img/product/featured-product/f-p-1.jpg" alt="">
                                            </a>
                                         
                                        </div>
                                        <div class="media-body">
                                            <a href="product-detail.php">
                                               <h4>Oxford Shirt</h4>
                                            <h5>$45.05</h5>
                                            </a>
                                         
                                        </div>
                                    </div>
                                    <div class="media">
                                        <div class="d-flex">
                                             <a href="product-detail.php">
                                            <img src="img/product/featured-product/f-p-2.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <a href="product-detail.php">
                                            <h4>Puffer Jacket</h4>
                                            <h5>$45.05</h5>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <div class="d-flex">
                                             <a href="product-detail.php">
                                            <img src="img/product/featured-product/f-p-3.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                             <a href="product-detail.php">
                                            <h4>Leather Bag</h4>
                                            <h5>$45.05</h5>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <div class="d-flex">
                                             <a href="product-detail.php">
                                            <img src="img/product/featured-product/f-p-4.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                             <a href="product-detail.php">
                                            <h4>Casual Shoes</h4>
                                            <h5>$45.05</h5>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <div class="fillter_slider_inner">
                                <ul class="portfolio_filter">
                                    <li class="active" data-filter="*"><a href="#">All</a></li>
                                    <?php foreach($menubarang as $keymenu=>$valmenu){?>
                                    <li data-filter=".<?=$valmenu->id?>"><a href="#"><?=$valmenu->keterangan?></a></li>
                                    
                                    <?php }?>
                                </ul>
                                <div class="fillter_slider owl-carousel">
                                <?php foreach($databarang2 as $keybrg2=>$valbrg2){?>
                                    <div class="item <?=$valbrg2->menu_barang?>">
                                        <div class="fillter_product_item bags">
                                            <div class="f_p_img">
                                                <a href="<?php echo \yii\helpers\Url::to(['/site/detail-product', 'id' => $valbrg2->id]); ?>">
                                                <img src="<?php echo Url::to('@web/dashboard/img/fotolatar/' . $valbrg2->latar_foto); ?>" alt="">
                                                <h5 class="sale">Sale</h5>
                                                </a>
                                                
                                            </div>
                                            <div class="f_p_text">
                                                  <a href="<?php echo \yii\helpers\Url::to(['/site/detail-product', 'id' => $valbrg2->id]); ?>">
                                                <h5><?=$valbrg2->nama_barang?></h5>
                                                
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                   <?php }?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
<!--================End Product_listing Area =================-->

<!--================Featured Product Area =================-->
<!-- <section class="feature_product_area">
            <div class="container">
                <div class="f_p_inner">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="f_product_left">
                                <div class="s_m_title">
                                    <h2>Featured Products</h2>
                                </div>
                                <div class="f_product_inner">
                                    <div class="media">
                                        <div class="d-flex">
                                            <a href="product-detail.php">
                                               <img src="img/product/featured-product/f-p-1.jpg" alt="">
                                            </a>
                                         
                                        </div>
                                        <div class="media-body">
                                            <a href="product-detail.php">
                                               <h4>Oxford Shirt</h4>
                                            <h5>$45.05</h5>
                                            </a>
                                         
                                        </div>
                                    </div>
                                    <div class="media">
                                        <div class="d-flex">
                                             <a href="product-detail.php">
                                            <img src="img/product/featured-product/f-p-2.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <a href="product-detail.php">
                                            <h4>Puffer Jacket</h4>
                                            <h5>$45.05</h5>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <div class="d-flex">
                                             <a href="product-detail.php">
                                            <img src="img/product/featured-product/f-p-3.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                             <a href="product-detail.php">
                                            <h4>Leather Bag</h4>
                                            <h5>$45.05</h5>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <div class="d-flex">
                                             <a href="product-detail.php">
                                            <img src="img/product/featured-product/f-p-4.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                             <a href="product-detail.php">
                                            <h4>Casual Shoes</h4>
                                            <h5>$45.05</h5>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <div class="fillter_slider_inner">
                                <ul class="portfolio_filter">
                                    <li class="active" data-filter="*"><a href="#">men's</a></li>
                                    <li data-filter=".woman"><a href="#">Woman</a></li>
                                    <li data-filter=".shoes"><a href="#">Shoes</a></li>
                                    <li data-filter=".bags"><a href="#">Bags</a></li>
                                </ul>
                                <div class="fillter_slider owl-carousel">
                                    <div class="item shoes">
                                        <div class="fillter_product_item bags">
                                            <div class="f_p_img">
                                                <a href="product-detail.php">
                                                <img src="img/product/fillter-product/f-product-1.jpg" alt="">
                                                <h5 class="sale">Sale</h5>
                                                </a>
                                                
                                            </div>
                                            <div class="f_p_text">
                                                  <a href="product-detail.php">
                                                <h5>Nike Max Air Vapor Power</h5>
                                                <h4>$45.05</h4>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item woman shoes bags">
                                        <div class="fillter_product_item">
                                            <div class="f_p_img">
                                                  <a href="product-detail.php">
                                                <img src="img/product/fillter-product/f-product-2.jpg" alt="">
                                                <h5 class="new">New</h5>
                                                </a>
                                            </div>
                                            <div class="f_p_text">
                                                  <a href="product-detail.php">
                                                <h5>Fossil Watch</h5>
                                                <h4><del>$250</del> $110</h4>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item woman shoes">
                                        <div class="fillter_product_item">
                                            <div class="f_p_img">
                                                  <a href="product-detail.php">
                                                <img src="img/product/fillter-product/f-product-3.jpg" alt="">
                                                <h5 class="discount">-10%</h5>
                                                </a>
                                            </div>
                                            <div class="f_p_text">
                                                  <a href="product-detail.php">
                                                <h5>High Heel</h5>
                                                <h4>$45.05</h4>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item shoes">
                                        <div class="fillter_product_item bags">
                                            <div class="f_p_img">
                                                  <a href="product-detail.php">
                                                <img src="img/product/fillter-product/f-product-1.jpg" alt="">
                                                <h5 class="sale">Sale</h5>
                                                </a>
                                            </div>
                                            <div class="f_p_text">
                                                  <a href="product-detail.php">
                                                <h5>Nike Max Air Vapor Power</h5>
                                                <h4>$45.05</h4>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item woman shoes bags">
                                        <div class="fillter_product_item">
                                            <div class="f_p_img">
                                                  <a href="product-detail.php">
                                                <img src="img/product/fillter-product/f-product-2.jpg" alt="">
                                                <h5 class="new">New</h5>
                                                </a>
                                            </div>
                                            <div class="f_p_text">
                                                  <a href="product-detail.php">
                                                <h5>Fossil Watch</h5>
                                                <h4><del>$250</del> $110</h4>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item woman shoes">
                                        <div class="fillter_product_item">
                                            <div class="f_p_img">
                                                  <a href="product-detail.php">
                                                <img src="img/product/fillter-product/f-product-3.jpg" alt="">
                                                <h5 class="discount">-10%</h5>
                                                </a>
                                            </div>
                                            <div class="f_p_text">
                                                  <a href="product-detail.php">
                                                <h5>High Heel</h5>
                                                <h4>$45.05</h4>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item shoes">
                                        <div class="fillter_product_item bags">
                                            <div class="f_p_img">
                                                <img src="img/product/fillter-product/f-product-1.jpg" alt="">
                                                <h5 class="sale">Sale</h5>
                                            </div>
                                            <div class="f_p_text">
                                                <h5>Nike Max Air Vapor Power</h5>
                                                <h4>$45.05</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item woman shoes bags">
                                        <div class="fillter_product_item">
                                            <div class="f_p_img">
                                                  <a href="product-detail.php">
                                                <img src="img/product/fillter-product/f-product-2.jpg" alt="">
                                                <h5 class="new">New</h5>
                                                </a>
                                            </div>
                                            <div class="f_p_text">
                                                  <a href="product-detail.php">
                                                <h5>Fossil Watch</h5>
                                                <h4><del>$250</del> $110</h4>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item woman shoes">
                                        <div class="fillter_product_item">
                                            <div class="f_p_img">
                                                  <a href="product-detail.php">
                                                <img src="img/product/fillter-product/f-product-3.jpg" alt="">
                                                <h5 class="discount">-10%</h5>
                                                </a>
                                            </div>
                                            <div class="f_p_text">
                                                  <a href="product-detail.php">
                                                <h5>High Heel</h5>
                                                <h4>$45.05</h4>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->
<!--================End Featured Product Area =================-->

<!--================Form Blog Area =================-->

<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
    Open modal
</button>